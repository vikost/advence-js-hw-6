"use strict";

// Асинхронність це виконання основного потоку коду, без затримки на очікування виконання задач,
// які виконуються у фоновому режимі

class FindIp {
  async getInfo() {
    try {
      const response = await fetch("https://api.ipify.org/?format=json");
      const ipAddress = await response.json();
      const infoResponse = await fetch(
        `http://ip-api.com/json/${ipAddress.ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`
      );
      const info = await infoResponse.json();
      return info;
    } catch (error) {
      console.error(error.message);
    }
  }
  render(infoObj) {
    return `<p>Континент:${infoObj.continent}</p>
    <p>Країна:${infoObj.country}</p>
    <p>Регіон:${infoObj.regionName}</p>
    <p>Місто:${infoObj.city}</p>
    <p>Район:${infoObj.district}</p>`;
  }
}

const findBtn = document.querySelector("#findIp");

findBtn.addEventListener("click", async () => {
  const ip = new FindIp();
  const infoObj = await ip.getInfo();
  document.querySelector(".find-ip").innerHTML = ip.render(infoObj);
});
